import {useState, useEffect} from "react";
import Joke from "./Joke_component/Joke";
import './App.css';

const url = 'https://api.chucknorris.io/jokes/random';

const App = () => {

  const [posts, setPosts] = useState([
        {value: "", id: '1'},
      ]
  );

    useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(url);

      if (response.ok) {
        const posts = await response.json();
        setPosts([posts]);
      }
    };

    fetchData().catch(e => console.error(e));
  }, []);

  return (
      <>
        <div className="Posts">
          {posts.map(post => (
              <Joke key={post.id}
                    value={post.value}
                    author={post.author}
              />
          ))}
        </div>
      </>
  );
};

export default App;
